import axios from "axios";
import { toast } from 'react-toastify';
import { CONFIG } from "../config/index";

export function fireAjax(method,url,data,auth){
    // let URL = CONFIG.API_BASE_URL + url;
    // let URL = CONFIG.API_URL + url;
    // let URL = "https://crossorigin.me/" + CONFIG.API_BASE_URL + url;
    let URL = "https://cors-anywhere.herokuapp.com/" + `${auth == undefined ? CONFIG.API_BASE_URL : CONFIG.API_LOGIN_URL }` + url;
    // let URL = "https://cors-anywhere.herokuapp.com/" + CONFIG.API_URL + url;
    // let URL = "http://localhost:8012/" + CONFIG.API_URL + url;


    let headers = {
        method: method,
        // body: data,
        body: JSON.stringify(data),
        headers: {
            'Content-Type': auth == undefined ?'application/json':'application/x-www-form-urlencoded',
            'Authorization':"Basic VVNFUl9DTElFTlRfQVBQOnBhc3N3b3Jk",
            'Accept': "application/json",
        }

    };

    // return axios({
    //     // headers: headers,
    //     method: method,
    //     url: URL,
    //     data: data
    // }).then(function (response) {
    //     console.log('^^^^^^^')
    //     console.log( response)
    //     // response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
    // }).then(function (response) {
    //     console.log('^^^^^^^')
    //     console.log( response)
    //     // response.data.pipe(fs.createWriteStream('ada_lovelace.jpg'))
    // });

    return fetch(URL, headers).then(response => {
        return response.json();
    });
}

export function notify(data){
    let type = data.type || ""
    let text = data.text || ""
    if( type == 'success' ){
        toast.success(text, {
            position: toast.POSITION.TOP_RIGHT
        });
    } else if( type == "error" ){
        toast.success(text, {
            position: toast.POSITION.TOP_RIGHT
        });
    } else if( type == "warning" ){
        toast.success(text, {
            position: toast.POSITION.TOP_RIGHT
        });
    } else if( type == "info" ){
        toast.success(text, {
            position: toast.POSITION.TOP_RIGHT
        });
    } else {
        toast(text, {
            position: toast.POSITION.TOP_RIGHT
        });
    }
}

export function getDataFromLocalStroage(key) {
    return  localStorage.getItem(key)
}

export function setDataInLocalStroage(key,value) {
    return localStorage.setItem(key , JSON.stringify(value));
}

export function yearInterval(){
    var array = []
    var year = new Date().getFullYear()
    for (var i = 0 ; i < 10 ; i++) {
        array.push(year-i)
    }
    return array;
}