import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import { AllCommunityModules } from "@ag-grid-community/all-modules";
import Typography from "@material-ui/core/Typography";
import SearchIcon from '@material-ui/icons/Search';

class ReceiptRowDetails extends React.Component {
  constructor(props) {
    super(props);

    let check_doAccountPlanSearchModeOn =
      props.doAccountPlanSearchModeOn || false;

    let columnDefs = [
      {
        headerName: "Account Code",
        field: "accountCode",
        cellRendererFramework: CustomAccountCode
      },
      { headerName: "Receipt Number", field: "receiptNumber" },
      { headerName: "Row Number", field: "rowNumber" },
      { headerName: "Debt Amount", field: "debtAmount" },
      { headerName: "Receivable Amount", field: "receivableAmount" }
    ];

    if (check_doAccountPlanSearchModeOn == false) {
      columnDefs.map((col, key) => {
        if (col.field == "accountCode") {
          delete columnDefs[key];
        }
      });
    }

    this.state = {
      modules: AllCommunityModules,
      columnDefs: columnDefs,
      rowData: []
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.receiptRowDetails !== prevState.rowData) {
      return { rowData: nextProps.receiptRowDetails };
    } else return null;
  }

  handleAccountCodeClick = data => {
    this.props.doAccountPlanSearchModeOn(data);
  };

  componentDidUpdate() {
    if (this.gridApi && this.gridApi.refreshCells) {
      this.gridApi.refreshCells();
    }
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };

  render() {
    const { onSelectReceiptHeader } = this.props;
    return (
      <div>
        <Typography variant="h6" gutterBottom>
          RECEIPT ROW DETAILS
        </Typography>
        <div
          className="ag-theme-balham"
          style={{ margin: "0 auto", height: "200px" }}
        >
          <AgGridReact
            modules={this.state.modules}
            onGridReady={this.onGridReady}
            enableSorting
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            // rowData={this.props.receiptRowDetails}
            onRowClicked={row => {
              if(onSelectReceiptHeader) onSelectReceiptHeader(row.data);
            }}
            gridOptions={{
              context: { componentParent: this }
            }}
          ></AgGridReact>
        </div>
      </div>
    );
  }
}

export default ReceiptRowDetails;

const CustomAccountCode = props => {
  const { context } = props;
  const handleAccountCodeClick = e => {
    e.stopPropagation();
    context.componentParent.handleAccountCodeClick(props.data);
  };
  let accountCode =  '';
  if(props.data.accountPlan && props.data.accountPlan.accountCode) {
    accountCode = props.data.accountPlan.accountCode;
  }
  return (
    <div className='d-flex justify-content-between'>
      {accountCode}
      <button 
        className='btn btn-primary btn-sm' 
        onClick={handleAccountCodeClick}
      >
        <SearchIcon />
      </button>
      {/* <input
        type="text"
        value={accountCode}
        onClick={handleAccountCodeClick}
        onChange={() => {}}
      /> */}
    </div>
  );
};
