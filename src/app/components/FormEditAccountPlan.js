import React, { useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import * as action from "../services/index";

const statusList = [
  {
    value: 1,
    label: "ACTIVE"
  },
  {
    value: 0,
    label: "PASSIVE"
  }
];

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing(1),
  },
}));

export default function FormEditAccountPlan(props) {
  let { data, onSave, onCancel, size, page } = props;
  
  const classes = useStyles();
  const [values, setValues] = React.useState({
    name: "Cat in the Hat",
    age: "",
    multiline: "Controlled",
    currency: "EUR"
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const [accountCode, setAccountCode] = useState(data.accountCode);
  const [accountName, setAccountName] = useState(data.accountName);
  const [debtAmount, setDebtAmount] = useState(data.debtAmount);
  const [creditBalance, setCreditBalance] = useState(data.creditBalance);
  const [status, setStatus] = useState(1);

  const handleSubmit = e => {
    e.preventDefault();
    let updatedAccountPlan = {
      accountPlanRef: data.id,
      companyPeriodRef: data.companyPeriodRef ? data.companyPeriodRef : data.companyPeriod.id,
      accountCode: accountCode,
      accountName: accountName,
      debtAmount: debtAmount,
      creditBalance: creditBalance,
      status: status * 1
    };
    onSave({updatedAccountPlan, size, page});
  };

  const handleCancel = e => {
    e.preventDefault();
    onCancel();
  }

  return (
    <form
      className={classes.container}
      onSubmit={handleSubmit}
      noValidate
      autoComplete="off"
    >
      <TextField
        label="Account Code"
        className={classes.textField}
        value={accountCode}
        onChange={handleChange("name")}
        margin="normal"
        variant="outlined"
        onChange={e => setAccountCode(e.target.value)}
      />
      <TextField
        label="Account Name"
        className={classes.textField}
        value={accountName}
        margin="normal"
        variant="outlined"
        onChange={e => setAccountName(e.target.value)}
      />
      <TextField
        type="number"
        label="Debt Amount"
        value={debtAmount}
        className={classes.textField}
        margin="normal"
        variant="outlined"
        onChange={e => setDebtAmount(e.target.value)}
      />
      <TextField
        type="number"
        label="Credit Balance"
        value={creditBalance}
        className={classes.textField}
        margin="normal"
        variant="outlined"
        onChange={e => setCreditBalance(e.target.value)}
      />
      <TextField
        id="outlined-select-currency-native"
        select
        label="Status"
        className={classes.textField}
        value={status}
        onChange={e => {
          setStatus(e.target.value);
        }}
        SelectProps={{
          native: true,
          MenuProps: {
            className: classes.menu
          }
        }}
        margin="normal"
        variant="outlined"
      >
        {statusList.map(option => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </TextField>
      <Button  variant="outlined"  type="submit" color="primary" className={classes.button}>
        Save
      </Button>
      <Button  variant="outlined"  onClick={handleCancel} color="primary" className={classes.button}>
        Cancel
      </Button>
    </form>
  );
}
