import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import Typography from "@material-ui/core/Typography";

class ReceiptHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columnDefs: [
        { headerName: "Receipt Type", field: "receiptType.code" },
        { headerName: "Receipt Date", field: "receiptDate" },
        { headerName: "Receipt Number", field: "receiptNumber" },
        { headerName: "Receipt Status", field: "receiptDate" }
      ],
      rowData: []
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.receiptHeaders != state.rowData) {
      return { rowData: props.receiptHeaders };
    }
    return null;
  }

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  };

  render() {
    const { selectedReceiptHeader } = this.props;
    return (
      <div>
        <Typography variant="h6" gutterBottom>
          RECEIPT HEADER
        </Typography>
        <div
          className="ag-theme-balham"
          style={{ margin: "0 auto", height: "200px" }}
        >
          <AgGridReact
            onGridReady={this.onGridReady}
            enableSorting
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            onRowClicked={row => {
              this.props.onSelectReceiptHeader(row.data);
            }}
            gridOptions={{
              rowSelection: "single"
            }}
          ></AgGridReact>
        </div>
      </div>
    );
  }
}

export default ReceiptHeader;
