import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { AgGridReact } from "ag-grid-react";
import { AllModules } from "@ag-grid-enterprise/all-modules";
import Button from "@material-ui/core/Button";
import * as action from "../services/index";
import {
  selectAccountPlan,
  updateValue,
  newDataInserted,
  userCompanyDetailRequest
} from "../redux/actions";
import cloneDeep from "lodash/cloneDeep";
import { Pagination, PaginationItem, PaginationLink, Input } from "reactstrap";

var preventDefault = e => e.preventDefault();

const CustomBtn = props => {
  const { context } = props;
  const handleEditClick = e => {
    e.stopPropagation();
    context.componentParent.handleEditClick(props.data);
  };
  const handleDeleteClick = e => {
    e.stopPropagation();
    context.componentParent.handleDeleteClick(props.data);
  };
  const isEditable =
    (context.componentParent.props &&
      context.componentParent.props.isEditable) ||
    false;
  return (
    <div>
      {!isEditable ? (
        <div>
          <Button
            style={{ padding: 0 }}
            variant="contained"
            size="small"
            color="primary"
            onClick={handleEditClick}
          >
            EDIT
          </Button>
          {/* <Button
        style={{ padding: 0 }}
        variant="contained"
        size="small"
        className="ml-2"
        onClick={handleDeleteClick}
      >
        DELETE
      </Button> */}
        </div>
      ) : null}
    </div>
  );
};

class AccountPlan extends React.Component {
  constructor(props) {
    super(props);

    let columnsToShow = props.columnsToShow || [];
    let columnDefs = [
      { headerName: "Account Code", field: "accountCode", comparator: this.textComparator },
      { headerName: "Account Name", field: "accountName", comparator: this.textComparator },
      { headerName: "Debt Amount", field: "debtAmount", comparator: this.textComparator },
      { headerName: "Credit Balance", field: "creditBalance", comparator: this.textComparator },
      {
        headerName: "Select",
        field: "select",
        cellRendererFramework: CustomBtn,
        editable: false
      }
    ];

    if (columnsToShow.length > 0) {
      columnDefs.map((col, key) => {
        if (columnsToShow.indexOf(col.field) == -1) {
          delete columnDefs[key];
        }
      });
    }

    this.state = {
      columnDefs: columnDefs,
      rowData: [],
      accountCode: "",
      modules: AllModules,
      getIndex: "",
      defaultColDef: {
        editable: true,
        sortable: true,
        filter: true
      },
      companyPeriodRef: "",
      totalPages: 0,
      totalElements: 0
    };
  }

  textComparator = () => {
    return 0;
  }

  componentDidUpdate(preprops) {
    var id = JSON.parse(action.getDataFromLocalStroage("companyPeriodRef"));
    if (this.state.accountCode !== this.props.accountSearch) {
      this.setState({ accountCode: this.props.accountSearch });
    }

    if (this.props.accountPlans.isSuccess && preprops.accountPlans.isSuccess !== this.props.accountPlans.isSuccess) {
      this.setState({
        rowData: Array.isArray(this.props.accountPlans.data) ? this.props.accountPlans.data :this.props.accountPlans.data.content,
        totalPages: this.props.accountPlans.data.totalPages,
        totalElements: this.props.accountPlans.data.totalElements
      });
    }
  }
  handleClickValue = data => {
    var index =
      this.state.rowData.length > 0 && this.state.rowData.indexOf(data);
    this.setState({ getIndex: index });
    this.props.selectAccountPlan(data);
  };

  handleEditClick = data => {
    this.props.doAccountPlanModalEditModeOn(data);
  };

  handleDeleteClick = data => {};

  onGridReady = params => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  };
  addRowDataUpdate = data => {
    var id = JSON.parse(action.getDataFromLocalStroage("companyPeriodRef"));
    var index =
      this.state.rowData.length > 0 && this.state.rowData.indexOf(data);
    var { limit: size, currentPage: page } = this.props;
    var newRow = {
      accountCode: "",
      accountName: "",
      companyPeriodRef: id,
      creditBalance: 0,
      debtAmount: 0
    };
    // this.props.doAccountPlanModalEditModeOn(newRow);
    // this.props.newDataInserted({ newRow, size, page });
    let clondRowData = cloneDeep(this.state.rowData);
    var updateRowValue = clondRowData.splice(index + 1, 0, newRow);
    this.setState({
      rowData: clondRowData
    });
  };
  handleUpdate = params => {
    var { limit: size, currentPage: page } = this.props;    
    const { data } = params;
    if(data.companyPeriod) {
      data.accountPlanRef = data.id;
      data.companyPeriodRef = data.companyPeriod.id;
      delete data.id;
      delete data.companyPeriod;
      this.props.updateValue(params.data);
    } else {
      this.props.newDataInserted({ updatedAccountPlan: params.data, size, page });
    }
  };
  updateRowData = params => {
    const { isEditable } = this.props;
    var result = !isEditable ? [
      {
        name: "Add New ",
        action: () => {
          this.addRowDataUpdate(params.node.data);
        }
      }
    ] : [];
    return result;
  };

  onChangePage(pageNumber, event) {
    event.preventDefault();
    this.props.onChangePage(pageNumber);
  }

  getContextMenuItems = params => this.updateRowData(params);

  render() {
    const {
      currentPage,
      limit,
      limits,
      onChangeLimit,
      isEditable
    } = this.props;
    const { totalPages, totalElements } = this.state;

    if (totalPages === 0) {
      return null;
    }
    const gridOptions = {
      context: { componentParent: this },
      rowSelection: "single",
      onCellEditingStopped: params => {
        if(params && params.colDef && params.colDef.field === 'creditBalance') {
          this.handleUpdate(params);
        }
      },
      onSortChanged: (a,b,c,d) => {
        const x = this.gridApi.sortController.getColumnsWithSortingOrdered()[0];
        if(x) {
          this.props.onSort(x.colId);
        }
      },
      sortingOrder: ['asc', 'desc']
    }
    // First and Prev button handlers and class.
    var prevHandler = preventDefault;
    var isNotFirst = currentPage > 0;
    if (isNotFirst) {
      prevHandler = this.onChangePage.bind(this, currentPage - 1);
    }
    // Next and Last button handlers and class.
    var nextHandler = preventDefault;
    var isNotLast = currentPage < totalPages - 1;
    if (isNotLast) {
      nextHandler = this.onChangePage.bind(this, currentPage + 1);
    }

    console.log(this.gridApi, this.gridColumnApi, "aaaaaaaaaaa");
    

    // var searchAccount =this.state.accountCode !== "" ? this.state.rowData.length>0 && this.state.rowData.filter((item)=> item.accountCode === this.state.accountCode) : null
    return (
      <div
        className="ag-theme-balham"
        style={{ margin: "30px auto", height: "300px", width: "800px" }}
      >
        <AgGridReact
          onGridReady={this.onGridReady}
          modules={this.state.modules}
          // enableSorting
          // enableServerSideSorting
          suppressClickEdit={isEditable}
          columnDefs={this.state.columnDefs}
          rowData={
            this.props.accountSearch && this.state.accountCode !== ""
              ? ""
              : this.state.rowData
          }
          onRowClicked={row => {
            this.handleClickValue(row.data);
          }}
          defaultColDef={this.state.defaultColDef}
          animateRows={true}
          onGridReady={this.onGridReady}
          enableRangeSelection={true}
          enableCellChangeFlash={true}
          allowContextMenuWithControlKey={true}
          getContextMenuItems={this.getContextMenuItems}
          gridOptions={gridOptions}
        ></AgGridReact>
        { !isEditable ?
        <div className="d-flex align-items-baseline pt-3">
          <Pagination
            aria-label="Page navigation example"
            // className={this.props.className}
          >
            <PaginationItem disabled={!isNotFirst}>
              <PaginationLink
                previous
                onClick={prevHandler}
                disabled={!isNotFirst}
              />
            </PaginationItem>
            <li className="align-self-center mx-2"> {currentPage + 1 } / {totalPages}</li>
            <PaginationItem disabled={!isNotLast}>
              <PaginationLink
                next
                onClick={nextHandler}
                disabled={!isNotLast}
              />
            </PaginationItem>
          </Pagination>
          <Input
            type="select"
            name="select"
            id="exampleSelect"
            onChange={onChangeLimit}
            value={limit}
            className="ml-2 form-control-sm pagination-limitor"
          >
            {limits.map((v, i) => (
              <option key={i} value={v}>{v}</option>
            ))}
          </Input>
          <div className="ml-2">Total {totalElements} Items</div>
        </div>
        : null }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    accountPlans: state.accountPlans.accountPlans,
    companyDetail: state.accountPlans.selectedCompntRef
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      selectAccountPlan,
      updateValue,
      newDataInserted,
      userCompanyDetailRequest
    },
    dispatch
  );
};

const VisibleAccountPlan = connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountPlan);

export default VisibleAccountPlan;
