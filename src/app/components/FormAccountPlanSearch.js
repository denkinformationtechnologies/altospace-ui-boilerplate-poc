import React, { useState } from "react";
import clsx from "clsx";
import Input from '@material-ui/core/Input'
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField"; 
import Button from "@material-ui/core/Button";
import AccontPlan from "./AccountPlan";
import * as action from "../services/index"

const statusList = [
  {
    value: 1,
    label: "ACTIVE"
  },
  {
    value: 0,
    label: "PASSIVE"
  }
];

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginRight: theme.spacing(1),
    marginTop:theme.spacing(0.5),
    paddingLeft:theme.spacing(0),
    padding:theme.spacing(0.5),
    width:theme.spacing(20)
  },
  inputFelid: {
    padding:theme.spacing(0.5),
    border:theme.spacing(0),
    width:theme.spacing(20)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing(1),
    height: theme.spacing(4),
    marginTop:theme.spacing(3.5)
  }
}));

export default function FormAccountPlanSearch(props) {
  let { data, onSave, onCancel, onSelect, selectedAccountPlan, onCreateAccountPlanRequest, selectedCompanyData, updateReceiptRowDetails } = props;

  const classes = useStyles();
  const [values, setValues] = React.useState({
    name: "Cat in the Hat",
    age: "",
    multiline: "Controlled",
    currency: "EUR"
  });

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  const [accountName, setAccountName] = useState("");
  const [accountSearch, searchAccountName] = useState("");
  const handleSubmit = e => {
    e.preventDefault();
    var id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'))
    let newAccountPlan = {
      "accountCode": "",
      "accountName": accountName,
      "companyPeriodRef":id, 
      "creditBalance": 0,
      "debtAmount": 0
    }
    onCreateAccountPlanRequest(newAccountPlan)
  };

  const handleCancel = e => {
    e.preventDefault();
    onCancel();
  };

  const handleSelect = e => {
    e.preventDefault();
    updateReceiptRowDetails();
  };

  return (
    <div>
      <form
        className={classes.container}
        onSubmit={handleSubmit}
        noValidate
        autoComplete="off"
      >
      <div className="d-flex">
      <div>
        <div className="mb-2">Account Code :</div>
        <div className="border rounded-pill p-1 mr-2">
          <i className="flaticon-search pl-1"></i>
        <input 
        className={classes.inputFelid}
        placeholder="search code auto complete"
        value={accountSearch}
        margin="normal"
        variant="outlined"
        onChange={e=>searchAccountName(e.target.value)}
        /></div>
      </div>
      <div>
        <div>Account Name :</div>
        <TextField
          label="Account Name"
          className={classes.textField + " text_area_feild"}
          value={accountName}
          margin="normal"
          variant="outlined"
          onChange={e => setAccountName(e.target.value)}
        />
        </div>
        <Button
          variant="outlined"
          type="submit"
          color="primary"
          className={classes.button}
        >
          Add
        </Button>
        </div>
        
      </form>
      <AccontPlan 
        columnsToShow={["accountCode", "accountName"]}
        onSelect={() => {
          this.props.onSelect();
        }}
        isEditable={true}
        accountSearch = {accountSearch}
      />
      { selectedAccountPlan != false ?
          <Button
            variant="outlined"
            type="submit"
            color="primary"
            className={classes.button}
            onClick={handleSelect}
          >
            Select
          </Button>
          : null
      }
        <Button
          variant="outlined"
          onClick={handleCancel}
          color="primary"
          className={classes.button}
        >
          Cancel
        </Button>
      
      
    </div>
  );
}
