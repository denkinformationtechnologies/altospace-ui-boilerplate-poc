import React, { Component } from 'react';
import { Button, Modal, Form } from "react-bootstrap";
import {
  userCompanyRequest,hideSideBar, logout , userCompanyDetailRequest
} from '../../redux/actions';
import { AgGridReact } from "ag-grid-react";
import { connect } from "react-redux";
import * as auth from "../../store/ducks/auth.duck";
import * as action from "../../services/index"
import Typography from "@material-ui/core/Typography";

const CustomBtn =props => {
  const event = props.agGridReact.props.handleClick
  return(
    <span className="select-information" onClick={()=>event(props.data)} > SELECT </span>
  )
}
class Listcompany extends Component {
    constructor(props) {
        super(props);
        this.columnDefs = [
          { headerName: "Company Name ", field: "companyName" },
          { headerName: "Company Description", field: "companyDesc" },
          { headerName: "Company Type", field: "companyType.desc" },
          {
            headerName: "Select",
            field: "",
            cellRendererFramework: CustomBtn
          }
        ];
        this.state = {
            smShow:false,
            companyList:'',
            signOut:false,
            filterName:'',
            search:"",
            year:"",
            selectCompany:''
        }
    }
    componentDidMount () {    
      const info = JSON.parse(action.getDataFromLocalStroage('info'));
      this.props.onCompanyList(info.domaincode);
    }
    componentDidUpdate() {
      if(this.props.listCompanyData !== this.state.companyList){
        this.setState({companyList:this.props.listCompanyData})
      }
      if(action.getDataFromLocalStroage('selectedCompany') != null && JSON.parse(action.getDataFromLocalStroage('companyPeriodRef')) !== null ) {
          this.props.history.push("/dashboard"); 
      }
    }
    componentWillUnmount() {
      this.props.onShowSideBar();
    }
    handleClick = (data)=> {
       
      action.setDataInLocalStroage('selectedCompany',data);

      this.setState({ smShow: true,selectCompany:data });
      this.props.getCompanyDetail(data.companyRef);
    }
    selectYear = () => {
      const { year: companyPeriodRef } = this.state;
      // var companyRefId = this.state.selectCompany && this.state.selectCompany.companyRef
      action.setDataInLocalStroage('companyPeriodRef', companyPeriodRef);
      this.setState({smShow:false});
      // this.props.getCompanyDetail(companyRefId)
    }
    
    handleSaveYear = e => {
      this.setState({year:e.target.value})
    }
    searchCompanyName = (data) => {
      var companyData = this.props.listCompanyData;
      const result = companyData.filter(company => company.companyName.toLowerCase().includes(data.toLowerCase()));
      this.setState({
        filterName:result,
        search:data
      })
    }
    handleClose = () => {
      this.setState({smShow:false, year: ''})
    }

    onGridReady = params => {
      this.gridApi = params.api;
      this.gridApi.sizeColumnsToFit();
    };

    handleSignOut = () => {
      localStorage.clear();
        this.props.history.push("/logout");
    }

    render() { 
      var authLogin = JSON.parse(action.getDataFromLocalStroage('persist:demo1-auth'));
      const { listPeriodData, listPeriodLoading } = this.props;
      const { year } = this.state;
        return (
            <div className="list_company_body  border">
                <Typography variant="h6" gutterBottom style={{ padding: 8 * 3 }}>
                >> COMPANY SELECT
                </Typography>
                <div className=" m-auto">
                <div className="mt-5">
                    <div className="px-5 mt-5 ">
                    <div className="py-3"> Please select a company </div>
                    <div className="border rounded-pill p-2 ">
                        <i className="flaticon-search pl-1 pr-3"></i>
                        <input 
                        className= " w-75 border-0"
                        placeholder="search"
                        margin="normal"
                        variant="outlined"
                        onChange={e=>this.searchCompanyName(e.target.value)}
                        />
                    </div>
                    </div>
                    <div className=" px-5 mt-4">
                    <div className="ag-theme-balham" style={{ margin: "0 auto", marginTop: "30px", height: "150px" }}>
                    <AgGridReact
                      onGridReady={this.onGridReady}
                      enableSorting
                      columnDefs={this.columnDefs}
                      rowData={this.state.search == "" ? this.state.companyList:this.state.filterName}
                      // onRowClicked={row => {
                      //   this.state.selectCompany(row.data);
                      // }}
                      gridOptions={{
                        context: { componentParent: this },
                        rowSelection: "single"
                      }}
                      handleClick={this.handleClick}
                    /></div>
                </div>
                <div>

                </div>
                <Modal show={this.state.smShow} onHide={this.handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title> Period  Select </Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                  {listPeriodLoading ? "loading..." :
                  <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>Year select</Form.Label>
                    <Form.Control as="select" onChange={this.handleSaveYear} value={ year }>
                      <option value=''>Select Year</option>
                      {listPeriodData.map((value, i)=>{
                        return(
                          <option key={i} value={value.companyPeriodRef}>{value.periodName}</option>
                        )
                      })}
                    </Form.Control>
                  </Form.Group>
                  }
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                      Close
                    </Button>
                    <Button disabled={!year} variant="primary" onClick={this.selectYear}>
                      Select
                    </Button>
                  </Modal.Footer>
                </Modal>
                <div className="d-flex justify-content-end">
                <Button className="mr-5 my-3" onClick={this.handleSignOut}> LOGOUT </Button>
                </div>

                </div>
                </div>
                
            </div>
        )
    }
}
const mapStateToProps = state => {                                                                                                                                                                                                                                                      
  return {
    listCompanyData: state.accountPlans.listCompany.data,
    listPeriodData: state.accountPlans.selectedCompntRef.data,
    listPeriodLoading: state.accountPlans.selectedCompntRef.isLoading
  };
};

const mapDispatchToProps = dispatch => ({
  ...auth.actions,
  onCompanyList: (domainCode) => {
  dispatch(userCompanyRequest(domainCode));
  },
  getCompanyDetail: data => {
    dispatch(userCompanyDetailRequest(data))
  },
  onShowSideBar: () => {
    dispatch(hideSideBar());
  },
  
 });
 export default connect(mapStateToProps, mapDispatchToProps)(Listcompany );
