import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import Typography from "@material-ui/core/Typography";
import ReceiptHeader from "../../../../components/ReceiptHeader";
import ReceiptRowDetails from "../../../../components/ReceiptRowDetails";
import Modal from "../../../../components/Modal";
import FormAccountPlanSearch from "../../../../components/FormAccountPlanSearch";
import * as action from "../../../../services/index"

 
import {
  listAccountPlanRequest,
  listReceiptHeaderRequest,
  selectReceiptHeader,
  listReceiptRowDetailsRequest,
  doAccountPlanSearchModeOn,
  doAccountPlanSearchModeOff,
  updateAccountCodeInReceiptRowDetails,
  updateReceiptRowDetailsRequest,
  createAccountPlanRequest
} from "../../../../redux/actions";

class ModalEditSearchableGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRow: ''
    }
  }
  componentDidMount() {
    var id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'))
    this.props.listReceiptHeaderRequest({
      id,
      fireDefaultRowDetailsRequest: true
    });
  }

  _onAccountPlanSelect = () => {
    this.props.updateAccountCodeInReceiptRowDetails({
      rowDetails: this.props.isModalAccountPlanSearchMode,
      accountPlan: this.props.selectedAccountPlan
    })
  }
  
  updateReceiptRowDetails = () => {
    const { selectedAccountPlan: { accountPlanRef }, receiptHeader: { data } } = this.props;
    const { selectedRow } = this.state;
    // delete selectedRow.receiptRow;
    // delete selectedRow.receiptNumber;
    // // delete selectedRow.receivableAmount;
    
    selectedRow.receiptRowRef = selectedRow.id;
    delete selectedRow.id;
    const row = {
      ...selectedRow,
      accountPlanRef: accountPlanRef,
      receiptHeaderRef: data && data[0] ? data[0].receiptHeaderId : '' ,
      rowNumber: 0,
      status: 1

    }
    this.props.updateReceiptRowDetailsRequest(row);
  }
  
  onSelectReceiptHeader = selectedRow => {
    this.setState({selectedRow});
  }

  render() {
    var selectedCompanyData = JSON.parse(action.getDataFromLocalStroage('selectedCompany')) 
    let receiptHeaders = this.props.receiptHeader.data;
    let receiptRowDetails = this.props.receiptRowDetails.data.content;
    return (
      <div>
        <Typography variant="h6" gutterBottom>
          > MODAL EDIT SEARCHABLE GRID - ACCOUNT
        </Typography>
        <ReceiptRowDetails
          receiptRowDetails={receiptRowDetails}
          doAccountPlanSearchModeOn={data => {
            var id = JSON.parse(action.getDataFromLocalStroage("companyPeriodRef"));
            this.props.doAccountPlanSearchModeOn(data);
            this.props.listAccountPlanRequest({dontSearch: true, id });
          }}
          onSelectReceiptHeader={this.onSelectReceiptHeader}
        />
        <Modal
          open={this.props.isModalAccountPlanSearchMode === false ? false : true}
          title="Account Plan Search"
          onClose={() => {
              this.setState({selectedRow: ''});
              this.props.doAccountPlanSearchModeOff();
          }}
        >
          <FormAccountPlanSearch
            selectedAccountPlan={this.props.selectedAccountPlan}
            onSelect={data => {
              this._onAccountPlanSelect();
            }}
            updateReceiptRowDetails = {this.updateReceiptRowDetails}
            onCancel={data => {
              this.setState({selectedRow: ''});
              this.props.doAccountPlanSearchModeOff();
            }}
            onCreateAccountPlanRequest={data => {
              this.props.createAccountPlanRequest(data)
            }}
            selectedCompanyData={selectedCompanyData}
          />
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    receiptHeader: state.receiptHeader.receiptHeader,
    receiptRowDetails: state.receiptRowDetails.receiptRowDetails,
    isModalAccountPlanSearchMode: state.receiptRowDetails.receiptRowDetails.isModalAccountPlanSearchMode,
    selectedAccountPlan: state.accountPlans.accountPlans.selectedAccountPlan,
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAccountPlanRequest,
      listReceiptHeaderRequest,
      selectReceiptHeader,
      listReceiptRowDetailsRequest,
      doAccountPlanSearchModeOn,
      doAccountPlanSearchModeOff,
      updateAccountCodeInReceiptRowDetails,
      updateReceiptRowDetailsRequest,
      createAccountPlanRequest
    },
    dispatch
  );
};

const VisibleModalEditSearchableGrid = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalEditSearchableGrid);

const RouterVisibleModalEditSearchableGrid = withRouter(
  VisibleModalEditSearchableGrid
);

export default RouterVisibleModalEditSearchableGrid;
