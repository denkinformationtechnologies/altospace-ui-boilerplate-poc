import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import MenuSidebar from "../../components/MenuSidebar";
import EditableGrid from "./editableGrid";
import MasterDetailGrid from "./masterDetailGrid";

import {
    userLoginRequest
} from '../../redux/actions';

class Dashboard extends React.Component {
    componentDidMount(){
        // console.log( this.props )
    }
    render() {
        return (
            <div>
                <div> Dashboard Page</div>
                <MenuSidebar/>
                <EditableGrid/>
                <MasterDetailGrid/>
            </div>
        )
    }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      userLoginRequest,
    },
    dispatch
  );
};

const VisibleDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

const RouterVisibleDashboard = withRouter(VisibleDashboard);

export default RouterVisibleDashboard;