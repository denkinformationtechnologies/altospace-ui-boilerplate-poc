import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from 'ag-grid-react';
import * as action from '../../../../services/index'
import Typography from '@material-ui/core/Typography';

import {
    listAccountPlanRequest,
    doAccountPlanModalEditModeOn,
    doAccountPlanModalEditModeOff,
    updateAccountPlanRequest
} from '../../../../redux/actions';

import AccountPlan from "../../../../components/AccountPlan"

class ReadOnlyWithModalGrid extends React.Component {

    componentDidMount(){
        var id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'))
        this.props.listAccountPlanRequest(id)
    }
    render() {
        return (
            <div>
                <Typography variant="h6" gutterBottom>
                    > READONLY WITH MODAL GRID - ACCOUNT PLAN
                </Typography> 
                <AccountPlan 
                    isEditable={true}
                    // doAccountPlanModalEditModeOn={(data) => {
                    //     this.props.doAccountPlanModalEditModeOn(data)
                    // }}
                    columnsToShow={["accountCode", "accountName","debtAmount","creditBalance"]}
                    isReadOnlyModal = {true}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    accountPlans: state.accountPlans.accountPlans,
    isModalEditMode: state.accountPlans.accountPlans.isModalEditMode
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listAccountPlanRequest,
      doAccountPlanModalEditModeOn,
      doAccountPlanModalEditModeOff,
      updateAccountPlanRequest
    },
    dispatch
  );
};

const VisibleReadOnlyWithModalGrid = connect(
  mapStateToProps,
  mapDispatchToProps
)(ReadOnlyWithModalGrid);

const RouterVisibleReadOnlyWithModalGrid = withRouter(VisibleReadOnlyWithModalGrid);

export default RouterVisibleReadOnlyWithModalGrid;