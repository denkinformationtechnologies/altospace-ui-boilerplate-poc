import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Typography from '@material-ui/core/Typography';

class WelcomeScreen extends React.Component {
    render() {
        return (
            <div>
                <Typography variant="h6" gutterBottom>
                    > WELCOME SCREEN
                </Typography>
            </div>
        )
    }
}

export default WelcomeScreen;