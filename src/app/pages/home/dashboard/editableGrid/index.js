import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import * as action from "../../../../services/index"
import Typography from "@material-ui/core/Typography";

import {
  setTabActive,
  listAccountPlanRequest,
  doAccountPlanModalEditModeOn,
  doAccountPlanModalEditModeOff,
  updateAccountPlanRequest,
  newDataInserted
} from "../../../../redux/actions";

import Modal from "../../../../components/Modal"
import AccountPlan from "../../../../components/AccountPlan";
import FormEditAccountPlan from "../../../../components/FormEditAccountPlan"

class EditableGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      size: 5,
      sort: '',
      sortColumn: ''
    }
  }
  onChangePage = (page) => {
    const { size } = this.state;
    const id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'));
    this.props.listAccountPlanRequest({id, page, size});
    this.setState({ page });

  }
  onChangeLimit = (e) => {
    const { page } = this.state;
    const id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'));
    this.props.listAccountPlanRequest({id, page, size: e.target.value});
    this.setState({size: e.target.value, page: 0});
  }

  onSort = (sortColumn) => {
    const id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'));
    const { sort, page, size } = this.state;
    let sorting = '';
    if(this.state.sortColumn === sortColumn) {
      sorting = sortColumn + ',' + (sort !== 'asc' ? 'asc' : 'desc');
      this.setState({ sort: sort !== 'asc' ? 'asc' : 'desc' });
      this.props.listAccountPlanRequest({ id, page, size, sorting });
    } else {
      sorting = sortColumn + ',asc';
      this.setState({sortColumn, sort: 'asc' });
      this.props.listAccountPlanRequest({ id, page, size, sorting });
    }
  }

  componentDidMount() {
    var id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'));
    const { page, size } = this.state;
    this.props.listAccountPlanRequest({id, page, size});
  }
  render() {
    const { page, size } = this.state;
    let accountPlans = this.props.accountPlans.data;
    return (
      <div>
        <Typography variant="h6" gutterBottom>
          > EDITABLE AG GRID - ACCOUNT PLAN
        </Typography>
        <AccountPlan
          onChangePage={this.onChangePage}
          onChangeLimit={this.onChangeLimit}
          onSort={this.onSort}
          currentPage={page}
          limit={size}
          doAccountPlanModalEditModeOn={(data) => {
            this.props.doAccountPlanModalEditModeOn(data)
          }}
          limits={[5, 10, 20]}
        />
        <Modal
          open={this.props.isModalEditMode === false ? false : true}
          title={!this.props.isModalEditMode.companyPeriodRef ? "Edit Modal" : "Add Modal"}
          onClose={() => {
              this.props.doAccountPlanModalEditModeOff()
          }}
        >
          <FormEditAccountPlan
            data={this.props.isModalEditMode}
            onSave={(data) => {
              !this.props.isModalEditMode.companyPeriodRef ?
                this.props.updateAccountPlanRequest(data)
              : this.props.newDataInserted(data)
            }}
            size={size}
            page={page}
            onCancel={(data) => {
                this.props.doAccountPlanModalEditModeOff()
            }}
          />
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    accountPlans: state.accountPlans.accountPlans,
    isModalEditMode: state.accountPlans.accountPlans.isModalEditMode
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setTabActive,
      listAccountPlanRequest,
      doAccountPlanModalEditModeOn,
      doAccountPlanModalEditModeOff,
      updateAccountPlanRequest,
      newDataInserted
    },
    dispatch
  );
};

const VisibleEditableGrid = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditableGrid);

const RouterVisibleEditableGrid = withRouter(VisibleEditableGrid);

export default RouterVisibleEditableGrid;
