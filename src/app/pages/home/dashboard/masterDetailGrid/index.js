import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AgGridReact } from "ag-grid-react";
import Typography from "@material-ui/core/Typography";
import ReceiptHeader from "../../../../components/ReceiptHeader";
import ReceiptRowDetails from "../../../../components/ReceiptRowDetails";
import * as action from "../../../../services/index"
import {
  listReceiptHeaderRequest,
  selectReceiptHeader,
  listReceiptRowDetailsRequest
} from "../../../../redux/actions";

class MasterDetailGrid extends React.Component {
  componentDidMount() {
    var id = JSON.parse(action.getDataFromLocalStroage('companyPeriodRef'))
    this.props.listReceiptHeaderRequest({id:id});
    
  }
  render() {
    let receiptHeaders = this.props.receiptHeader.data;
    let receiptRowDetails = this.props.receiptRowDetails.data.content;
    let selectedReceiptHeader = this.props.receiptHeader.selectedReceiptHeader;
    return (
      <div>
        <Typography variant="h6" gutterBottom>
          > MASTERDETAIL GRID READONLY - ACCOUNT
        </Typography>
        <ReceiptHeader
          selectedReceiptHeader={selectedReceiptHeader}
          receiptHeaders={receiptHeaders}
          onSelectReceiptHeader={selectedReceiptHeader => {
            this.props.selectReceiptHeader(selectedReceiptHeader);
            this.props.listReceiptRowDetailsRequest(selectedReceiptHeader);
          }}
        />
        <br />
        <ReceiptRowDetails receiptRowDetails={receiptRowDetails} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    receiptHeader: state.receiptHeader.receiptHeader,
    receiptRowDetails: state.receiptRowDetails.receiptRowDetails
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      listReceiptHeaderRequest,
      selectReceiptHeader,
      listReceiptRowDetailsRequest
    },
    dispatch
  );
};

const VisibleMasterDetailGrid = connect(
  mapStateToProps,
  mapDispatchToProps
)(MasterDetailGrid);

const RouterVisibleMasterDetailGrid = withRouter(VisibleMasterDetailGrid);

export default RouterVisibleMasterDetailGrid;
