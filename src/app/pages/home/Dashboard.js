import React, {useEffect} from "react";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Link from '@material-ui/core/Link';
import * as action from "../../services/index"
import WelcomeScreen from "./dashboard/welcomeScreen";
import EditableGrid from "./dashboard/editableGrid";
import MasterDetailGrid from "./dashboard/masterDetailGrid";
import ReadOnlyWithModalGrid from "./dashboard/readOnlyWithModalGrid";
import ModalEditSearchableGrid from "./dashboard/modalEditSearchableGrid";

import { setTabActive, setTabInActive } from "../../redux/actions";

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dir: PropTypes.string.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    flexGrow: 1
    // height:"100%"
  }
}));

function Dashboard(props) {
  const { tabs, history, setTabActive, setTabInActive, activeTab } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState("");
  // var authLogin = JSON.parse(action.getDataFromLocalStroage('selectedCompany') != null) && JSON.parse(action.getDataFromLocalStroage('selectedYear')) !== null
  // if(!authLogin ) {
  //   history.push("/listcompany");
  //   return 
  // }
  // else{
  function handleChange(event, newValue) {
    setValue(newValue);
    if (activeTab != newValue) {
      history.push("/dashboard/" + newValue);
    }
  }  

  function handleChangeIndex(index) {
    setValue(index);
  }

  function handleTabClose(tabName) {
    setTabInActive(tabName);
    // setTabActive(firstActiveTab.name);
    // history.push("/dashboard/" + firstActiveTab.name);
  }
  if (history && history.location && history.location.pathname) {
    let path = history.location.pathname;
    let selectedTab = "";
    if (path.indexOf("welcomeScreen") != -1) {
      selectedTab = "welcomeScreen";
    } else if (path.indexOf("editableGrid") != -1) {
      selectedTab = "editableGrid";
    } else if (path.indexOf("masterDetailGrid") != -1) {
      selectedTab = "masterDetailGrid";
    } else if (path.indexOf("readOnlyWithModalGrid") != -1) {
      selectedTab = "readOnlyWithModalGrid";
    } else if (path.indexOf("modalEditSearchableGrid") != -1) {
      selectedTab = "modalEditSearchableGrid";
    }
    if (selectedTab != "" && activeTab != selectedTab) {
      setTabActive(selectedTab);
    }
    if( value != selectedTab ){
      setValue(selectedTab)
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
        >
          {tabs.map(tab => {
            return tab.active ? (
              <Tab
                key={tab.name}
                value={tab.name}
                // label={tab.text}
                label={
                  <>
                    {activeTab == tab.name ? (
                      <Link
                        // raised={true}
                        // color="accent"
                        onClick={() => handleTabClose(tab.name)}
                      >
                        X
                      </Link>
                    ) : null}
                    {tab.text}
                  </>
                }
              />
            ) : null;
          })}
        </Tabs>
      </AppBar>
      <TabContainer dir={theme.direction}>
        {activeTab == "welcomeScreen" ? (
          <WelcomeScreen />
        ) : activeTab == "editableGrid" ? (
          <EditableGrid />
        ) : activeTab == "masterDetailGrid" ? (
          <MasterDetailGrid />
        ) : activeTab == "readOnlyWithModalGrid" ? (
          <ReadOnlyWithModalGrid />
        ) : activeTab == "modalEditSearchableGrid" ? (
          <ModalEditSearchableGrid />
        ) : (
          <WelcomeScreen />
        )}
      </TabContainer>
    </div>
  );
}
// }

function mapStateToProps(state) {
  return {
    tabs: state.tabs.tabs,
    activeTab: state.tabs.activeTab
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setTabActive,
      setTabInActive
    },
    dispatch
  );
};

const VisibleDashboard = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

const RouterVisibleDashboard = withRouter(VisibleDashboard);

export default RouterVisibleDashboard;
