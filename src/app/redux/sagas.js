import { all, takeLatest } from "redux-saga/effects";
import * as constants from "./constants";

import * as auth from "./ducks/auth.duck";

import { loginRequest } from "./auth/actions";
import {
  listAccountPlanRequest,
  updateAccountPlanRequest,
  createAccountPlanRequest,
  userCompanyRequest,
  userCompanyDetailRequest,
  updateValue,
  newDataInserted
} from "./accountPlans/actions";
import { listReceiptHeaderRequest } from "./receiptHeader/actions";
import { listReceiptRowDetailsRequest, updateReceiptRowDetailsRequest } from "./receiptRowDetails/actions";

export function* watchActions() {
  yield takeLatest(constants.USER_LOGIN_REQUEST, loginRequest);

  yield takeLatest(constants.USER_COMPANY_REQUEST, userCompanyRequest);

  yield takeLatest(constants.USER_COMPANY_DETAIL_REQUEST, userCompanyDetailRequest);

  yield takeLatest(constants.LIST_ACCOUNT_PLAN_REQUEST, listAccountPlanRequest);

  yield takeLatest(constants.UPDATE_VALUE, updateValue);

  yield takeLatest(constants.NEW_DATA_INSERTED, newDataInserted);

  yield takeLatest(
    constants.LIST_RECEIPT_HEADER_REQUEST,
    listReceiptHeaderRequest
  );

  yield takeLatest(
    constants.LIST_RECEIPT_ROW_DETAILS_REQUEST,
    listReceiptRowDetailsRequest
  );

  yield takeLatest(
    constants.UPDATE_RECEIPT_ROW_DETAILS_REQUEST,
    updateReceiptRowDetailsRequest
  );

  yield takeLatest(
    constants.UPDATE_ACCOUNT_PLAN_REQUEST,
    updateAccountPlanRequest
  );

  yield takeLatest(
    constants.CREATE_ACCOUNT_PLAN_REQUEST,
    createAccountPlanRequest
  );
}

export default function* rootSaga() {
  yield all([auth.saga(), watchActions()]);
}
