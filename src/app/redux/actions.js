import { createAction } from "redux-actions";
import * as constants from "../redux/constants";

export const setTabActive = createAction(constants.SET_TAB_ACTIVE)
export const setTabInActive = createAction(constants.SET_TAB_INACTIVE)

export const userLoginRequest = createAction(constants.USER_LOGIN_REQUEST);
export const userLoginSuccess = createAction(constants.USER_LOGIN_SUCCESS);
export const userLoginError = createAction(constants.USER_LOGIN_ERROR);

export const userCompanyRequest = createAction(constants.USER_COMPANY_REQUEST);
export const userCompanySuccess = createAction(constants.USER_COMPANY_SUCCESS);
export const userCompanyError = createAction(constants.USER_COMPANY_ERROR);

export const userCompanyDetailRequest = createAction(constants.USER_COMPANY_DETAIL_REQUEST);
export const userCompanyDetailSuccess = createAction(constants.USER_COMPANY_DETAIL_SUCCESS);
export const userCompanyDetailError = createAction(constants.USER_COMPANY_DETAIL_ERROR);

export const hideSideBar = createAction(constants.SIDE_BAR_CLOSE); 
export const updateValue = createAction(constants.UPDATE_VALUE);
export const newDataInserted = createAction(constants.NEW_DATA_INSERTED);

export const listAccountPlanRequest = createAction(constants.LIST_ACCOUNT_PLAN_REQUEST)
export const listAccountPlanSuccess = createAction(constants.LIST_ACCOUNT_PLAN_SUCCESS)
export const listAccountPlanError = createAction(constants.LIST_ACCOUNT_PLAN_ERROR)

export const listReceiptHeaderRequest = createAction(constants.LIST_RECEIPT_HEADER_REQUEST);
export const listReceiptHeaderSuccess = createAction(constants.LIST_RECEIPT_HEADER_SUCCESS);
export const listReceiptHeaderError = createAction(constants.LIST_RECEIPT_HEADER_ERROR);

export const selectReceiptHeader = createAction(constants.SELECT_RECEIPT_HEADER);

export const listReceiptRowDetailsRequest = createAction(constants.LIST_RECEIPT_ROW_DETAILS_REQUEST)
export const listReceiptRowDetailsSuccess = createAction(constants.LIST_RECEIPT_ROW_DETAILS_SUCCESS)
export const listReceiptRowDetailsError = createAction(constants.LIST_RECEIPT_ROW_DETAILS_ERROR)

export const updateReceiptRowDetailsRequest = createAction(constants.UPDATE_RECEIPT_ROW_DETAILS_REQUEST)
export const updateReceiptRowDetailsSuccess = createAction(constants.UPDATE_RECEIPT_ROW_DETAILS_SUCCESS)
export const updateReceiptRowDetailsError = createAction(constants.UPDATE_RECEIPT_ROW_DETAILS_ERROR)

export const doAccountPlanModalEditModeOn = createAction(constants.DO_ACCOUNT_PLAN_MODAL_EDIT_MODE_ON)
export const doAccountPlanModalEditModeOff = createAction(constants.DO_ACCOUNT_PLAN_MODAL_EDIT_MODE_OFF)

export const updateAccountPlanRequest = createAction(constants.UPDATE_ACCOUNT_PLAN_REQUEST)
export const updateAccountPlanSuccess = createAction(constants.UPDATE_ACCOUNT_PLAN_SUCCESS)
export const updateAccountPlanError = createAction(constants.UPDATE_ACCOUNT_PLAN_ERROR)

export const doAccountPlanSearchModeOn = createAction(constants.DO_ACCOUNT_PLAN_SEARCH_MODE_ON)
export const doAccountPlanSearchModeOff = createAction(constants.DO_ACCOUNT_PLAN_SEARCH_MODE_OFF)

export const selectAccountPlan = createAction(constants.SELECT_ACCOUNT_PLAN)

export const updateAccountCodeInReceiptRowDetails = createAction(constants.UPDATE_ACCOUNT_CODE_IN_RECEIPT_ROW_DETAILS)

export const createAccountPlanRequest = createAction(constants.CREATE_ACCOUNT_PLAN_REQUEST)
export const createAccountPlanSuccess = createAction(constants.CREATE_ACCOUNT_PLAN_SUCCESS)
export const createAccountPlanError = createAction(constants.CREATE_ACCOUNT_PLAN_ERROR)