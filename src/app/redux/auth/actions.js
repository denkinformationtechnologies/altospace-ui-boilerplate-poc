import {call, put} from 'redux-saga/effects';
import * as actions from '../actions';
import {fireAjax,setDataInLocalStroage} from '../../services';
import { login } from "../../crud/auth.crud";

export function* loginRequest (action) {
    try {
      const response = yield call(fireAjax, 'POST', 
                  `/iam/oauth/token?domaincode=${action.payload.domainCode}&username=${action.payload.username}&password=${action.payload.password}&grant_type=password`, {} ,{login:true}
      );
        if (response && response.access_token) {
          yield setDataInLocalStroage('token',response.access_token);
          yield setDataInLocalStroage('info',response);
          yield put(actions.userLoginSuccess(response));
          // yield put (login(response.access_token))
        } else {
          yield put(actions.userLoginError(response.error));
        }
    } catch (e) {
        yield put(actions.userLoginError('Error Occurs !!'));
        console.warn('Some error found in "logingRequest" action\n', e);
    }
}