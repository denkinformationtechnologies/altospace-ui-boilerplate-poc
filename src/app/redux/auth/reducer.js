import {handleActions} from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../constants';

let initialState = {
  login: {
    isLoading:  false,
    isLoggedIn: false,
    isSuccess:  false,
    isError:    false,
    data:    ''
  }
};

const userLoginRequest = (state, action) => {
    return update(state, {
      login: {
        isLoading: {$set: true},
        isLoggedIn: { $set: false },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: "" }
      }
    });
}

const userLoginSuccess = (state, action) => {
  return update(state, {
    login: {
      isLoading: {$set: false},
      isLoggedIn: { $set: true },
      isSuccess: { $set: true },
      isError: { $set: false },
      data: { $set: action.payload }
    }
  });
}

const userLoginError = (state, action) => {
  return update(state, {
    login: {
      isLoading: {$set: false},
      isLoggedIn: { $set: false },
      isSuccess: { $set: false },
      isError: { $set: true },
      data: { $set: action.payload }
    }
  });
}

export default handleActions({
  [constants.USER_LOGIN_REQUEST]: userLoginRequest,
  [constants.USER_LOGIN_SUCCESS]: userLoginSuccess,
  [constants.USER_LOGIN_ERROR]: userLoginError,
}, initialState);