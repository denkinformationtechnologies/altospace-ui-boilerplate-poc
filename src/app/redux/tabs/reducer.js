import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../constants";

let initialState = {
  activeTab: "welcomeScreen",
  tabs: [
    {
      name: "welcomeScreen",
      text: "WELCOME SCREEN",
      active: false
    },
    {
      name: "editableGrid",
      text: "EDITABLE GRID",
      active: false
    },
    {
      name: "masterDetailGrid",
      text: "MASTERDETAIL GRID",
      active: false
    },
    {
      name: "readOnlyWithModalGrid",
      text: "READONLY WITH MODAL GRID",
      active: false
    },
    {
      name: "modalEditSearchableGrid",
      text: "MODAL EDIT SEARCHABLE GRID",
      active: false
    }
  ]
};

const setTabActive = (state, action) => {
  let selectedTab = action.payload;
  let { tabs } = state;
  tabs.map(tab => {
    if (tab.name == selectedTab) {
      tab["active"] = true;
    }
    return tab;
  });
  return update(state, {
    activeTab: { $set: selectedTab },
    tabs: { $set: tabs }
  });
};

const setTabInActive = (state, action) => {
  let selectedTab = action.payload;
  let { tabs } = state;
  tabs.map(tab => {
    if (tab.name == selectedTab) {
      tab["active"] = false;
    }
    return tab;
  });
  return update(state, {
    tabs: { $set: tabs }
  });
};

export default handleActions(
  {
    [constants.SET_TAB_ACTIVE]: setTabActive,
    [constants.SET_TAB_INACTIVE]: setTabInActive
  },
  initialState
);
