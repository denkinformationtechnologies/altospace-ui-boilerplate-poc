import {combineReducers} from 'redux';

import * as auth from "./ducks/auth.duck";
import { metronic } from "../../_metronic";
import authReducer from './auth/reducer';
import tabs from "./tabs/reducer"
import accountPlans from "./accountPlans/reducer"
import receiptHeader from "./receiptHeader/reducer"
import receiptRowDetails from "./receiptRowDetails/reducer"


export default combineReducers({
    // auth
    auth: auth.reducer,
    i18n: metronic.i18n.reducer,
    builder: metronic.builder.reducer,
    tabs: tabs,
    accountPlans: accountPlans,
    receiptHeader: receiptHeader,
    receiptRowDetails: receiptRowDetails,
    authReducer: authReducer
})