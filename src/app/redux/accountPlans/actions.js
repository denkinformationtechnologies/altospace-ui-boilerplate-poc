import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { fireAjax, notify , setDataInLocalStroage } from "../../services";

export function* userCompanyRequest(action) {
  try {
    const response = yield call(
      fireAjax,
      "POST",
      "/customermanagement/list-company-by-domain",
      {
        domainRef: action.payload
      }
    );
    if (response.data) {
      yield put(actions.userCompanySuccess(response.data));
    }

  } catch (e) {
    console.log(e);
    yield put(actions.userCompanyError(e));
  }
}

export function* userCompanyDetailRequest(action) {
  try {
    const response = yield call(
      fireAjax,
      "POST",
      "/customermanagement/list-period-by-company",
      {
        companyRef: `${action.payload}`
      }
    );
    if (response.data) {
      // var id = response.data[0].companyPeriodRef
      // setDataInLocalStroage('companyPeriodRef', id)
      yield put(actions.userCompanyDetailSuccess(response.data));
    } else {
      yield put(actions.userCompanyDetailError(response));
    }

  } catch (e) {
    console.log(e);
    yield put(actions.userCompanyDetailError(e));
  }
}


export function* listAccountPlanRequest(action) { 
  // 70a7c2cc-17bc-4697-acdd-dbde14e002fd
  // 6be7c05c-e300-432c-9960-c1c7983722c3
  // 6be7c05c-e300-432c-9960-c1c7983722c3
  //7009ae3e-84a9-47b5-9593-9986260f118d
  const { size, page, id, dontSearch, sorting : sort } = action.payload;
  try {
    const response = yield call(
      fireAjax,
      "POST",
      dontSearch ? "/poc-management/list-accountplan-by-companyperiod"
      : `/poc-management/search-accountplan?page=${page}&size=${size}&search=true${sort ? '&sort=' + sort : '' }`,
      {
        companyPeriodRef: id,
        accountCode: null,
        accountName: null,
        status: null
      }
    );
    if (response.data) {
      yield put(actions.listAccountPlanSuccess(response.data));
    }

    // if (response.error === 0) {
    //   let {token, userid} = response.data;
    //   yield put(actions.userLoginSuccess());
    // } else if (response.error === 1) {
    //   yield put(actions.userLoginError());
    // }
  } catch (e) {
    console.log(e);
    // yield put(actions.userLoginError('Error Occurs !!'));
    // console.warn('Some error found in "logingRequest" action\n', e);
  }
}

export function* updateAccountPlanRequest(action) {
  try {
    const {size, page , updatedAccountPlan, companyPeriodRef} = action.payload;
    const response = yield call(
      fireAjax,
      "PUT",
      "/poc-management/update-accountplan",
      updatedAccountPlan
    );

    if (response && response.code * 1 == 0 ) {
      notify({
        type:"success",
        text: response.message
      });
      yield put(actions.updateAccountPlanSuccess(response.data));
      yield put(actions.doAccountPlanModalEditModeOff());
      yield put(actions.listAccountPlanRequest({id: companyPeriodRef, size, page}));
    }

    // if (response.error === 0) {
    //   let {token, userid} = response.data;
    //   yield put(actions.userLoginSuccess());
    // } else if (response.error === 1) {
    //   yield put(actions.userLoginError());
    // }
  } catch (e) {
    console.log(e);
    // yield put(actions.userLoginError('Error Occurs !!'));
    // console.warn('Some error found in "logingRequest" action\n', e);
  }
}


export function* createAccountPlanRequest(action) {

  try {
    const response = yield call(
      fireAjax,
      "POST",
      "/poc-management/create-accountplan",
      action.payload
    );

    if (response && response.code * 1 == 0 ) {
      notify({
        type:"success",
        text: response.message
      });
      yield put(actions.createAccountPlanSuccess(response.data));
      yield put(actions.listAccountPlanRequest(action.payload.companyPeriodRef));
    }

    // if (response.error === 0) {
    //   let {token, userid} = response.data;
    //   yield put(actions.userLoginSuccess());
    // } else if (response.error === 1) {
    //   yield put(actions.userLoginError());
    // }
  } catch (e) {
    console.log(e);
    // yield put(actions.userLoginError('Error Occurs !!'));
    // console.warn('Some error found in "logingRequest" action\n', e);
  }
}

export function* updateValue(action) {
  try {
    action.payload.status=0
    const response = yield call(
      fireAjax,
      "PUT",
      "/poc-management/update-accountplan",
      action.payload
    );

    // if (response && response.code * 1 == 0 ) {
    //   notify({
    //     type:"success",
    //     text: response.message
    //   });
    //   yield put(actions.createAccountPlanSuccess(response.data));
    //   yield put(actions.listAccountPlanRequest(action));
    // }

    // if (response.error === 0) {
    //   let {token, userid} = response.data;
    //   yield put(actions.userLoginSuccess());
    // } else if (response.error === 1) {
    //   yield put(actions.userLoginError());
    // }
  } catch (e) {
    console.log(e);
    // yield put(actions.userLoginError('Error Occurs !!'));
    // console.warn('Some error found in "logingRequest" action\n', e);
  }
}

export function* newDataInserted(action) { 

  try {
    const { updatedAccountPlan: newRow, size, page} = action.payload;
    const response = yield call(
      fireAjax,
      "POST",
      "/poc-management/create-accountplan",
      newRow
    );

    if (response && response.code * 1 == 0 ) {
      notify({
        type:"success",
        text: response.message
      });
      yield put(actions.createAccountPlanSuccess(response.data));
      yield put(actions.doAccountPlanModalEditModeOff());
      yield put(actions.listAccountPlanRequest({id: newRow.companyPeriodRef, size, page}));
    }

    // if (response.error === 0) {
    //   let {token, userid} = response.data;
    //   yield put(actions.userLoginSuccess());
    // } else if (response.error === 1) {
    //   yield put(actions.userLoginError());
    // }
  } catch (e) {
    console.log(e);
    // yield put(actions.userLoginError('Error Occurs !!'));
    // console.warn('Some error found in "logingRequest" action\n', e);
  }
}

