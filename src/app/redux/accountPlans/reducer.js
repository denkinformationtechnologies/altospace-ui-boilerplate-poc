import {handleActions} from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../constants';

let initialState = {
  accountPlans: {
    isLoading:  false,
    data: [],
    isSuccess:  false,
    isError:    false,
    message:    '',
    isModalEditMode: false,
    selectedAccountPlan: false
  },
  createAccountPlan:{
    isLoading:  false,
    isSuccess:  false,
    isError:    false,
    message:    '',
  },
  listCompany:{
    isLoading: false,
    isSuccess: false,
    isError: false,
    message:'',
    onHide:false,
    data: []
  },
  selectedCompntRef: {
    isLoading:false,
    isSuccess:false,
    isError:false,
    data:[]
  }
};

const listAccountPlanRequest = (state, action) => {
    return update(state, {
      accountPlans: {
        isLoading: {$set: true},
        data: { $set: [] },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: "" },
        selectedAccountPlan: {$set: false}
      }
    });
}

const listAccountPlanSuccess = (state, action) => {
    return update(state, {
      accountPlans: {
        isLoading: {$set: false},
        data: { $set: action.payload },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: "" },
        selectedAccountPlan: {$set: false}
      }
    });
}

const doAccountPlanModalEditModeOn = (state, action) => {
    return update(state, {
        accountPlans: {
            isModalEditMode: { $set: action.payload },
        }
    });
}

const doAccountPlanModalEditModeOff = (state, action) => {
    return update(state, {
        accountPlans: {
            isModalEditMode: { $set: false },
        }
    });
}

const selectAccountPlan = (state, action) => {
  return update(state, {
    accountPlans: {
      selectedAccountPlan: { $set: action.payload }
    }
});
}

const createAccountPlanRequest = (state, action) => {
  return update(state, {
    createAccountPlan: {
      isLoading: {$set: true},
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: "" },
    }
  });
}

const createAccountPlanSuccess = (state, action) => {
  return update(state, {
    createAccountPlan: {
      isLoading: {$set: false},
      isSuccess: { $set: true },
      isError: { $set: false },
      message: { $set: "" },
    }
  });
}

const userCompanyRequest = (state, action) => {
  return update(state, {
    listCompany: {
      isLoading: {$set: true},
      onHide: {$set:true}
    }
  });
}

const userCompanySuccess = (state, action) => {
  return update(state, {
    listCompany: {
      isLoading: {$set: false},
      isSuccess: { $set: true },
      isError: { $set: false },
      message: { $set: "Successfully" },
      data: {$set: action.payload}
    }
  });
}

const userCompanyError = (state, action) => {
  return update(state, {
    listCompany: {
      isLoading: {$set: false},
      isSuccess: { $set: true },
      isError: { $set: true },
      message: { $set: "Error Occur" }
    }
  });
}

const userCompanyDetailRequest = (state, action) => {
  return update(state, {
    selectedCompntRef: {
      isLoading:{$set: true},
      isSuccess:{$set: false},
      isError:{$set: false},
    }
  });
}

const userCompanyDetailSuccess = (state, action) => {
  return update(state, {
    selectedCompntRef: {
      isLoading:{$set: false},
      isSuccess:{$set: true},
      isError:{$set: false},
      data:{$set: action.payload}
    }
  });
}

const userCompanyDetailError = (state, action) => {
  return update(state, {
    selectedCompntRef: {
      isLoading:{$set: false},
      isSuccess:{$set: false},
      isError:{$set: true},
      data: {$set: []}
    }
  });
}

const hideSideBar = (state, action) => {
  return update(state, {
    listCompany: {
      onHide: {$set:false}
    }
  });
}


export default handleActions({
  [constants.LIST_ACCOUNT_PLAN_REQUEST]: listAccountPlanRequest,
  [constants.LIST_ACCOUNT_PLAN_SUCCESS]: listAccountPlanSuccess,
  [constants.DO_ACCOUNT_PLAN_MODAL_EDIT_MODE_ON]: doAccountPlanModalEditModeOn,
  [constants.DO_ACCOUNT_PLAN_MODAL_EDIT_MODE_OFF]: doAccountPlanModalEditModeOff,
  [constants.SELECT_ACCOUNT_PLAN]: selectAccountPlan,
  [constants.CREATE_ACCOUNT_PLAN_REQUEST]: createAccountPlanRequest,
  [constants.CREATE_ACCOUNT_PLAN_SUCCESS]: createAccountPlanSuccess,
  [constants.USER_COMPANY_REQUEST]: userCompanyRequest,
  [constants.USER_COMPANY_SUCCESS]: userCompanySuccess,
  [constants.USER_COMPANY_ERROR]: userCompanyError,
  [constants.USER_COMPANY_DETAIL_REQUEST]: userCompanyDetailRequest,
  [constants.USER_COMPANY_DETAIL_SUCCESS]: userCompanyDetailSuccess,
  [constants.USER_COMPANY_DETAIL_ERROR]: userCompanyDetailError,
  [constants.SIDE_BAR_CLOSE]: hideSideBar
}, initialState);