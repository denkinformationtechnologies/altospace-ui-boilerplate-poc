import {handleActions} from 'redux-actions';
import update from 'immutability-helper';
import * as constants from '../constants';

let initialState = {
  receiptHeader: {
    isLoading:  false,
    data: [],
    isSuccess:  false,
    isError:    false,
    message:    '',
    selectedReceiptHeader: false
  }
};

const listReceiptHeaderRequest = (state, action) => {
    return update(state, {
      receiptHeader: {
        isLoading: {$set: true},
        data: { $set: [] },
        isSuccess: { $set: false },
        isError: { $set: false },
        message: { $set: "" },
        message: { $set: "" },
        selectedReceiptHeader: { $set: false }
      }
    });
}

const listReceiptHeaderSuccess = (state, action) => {
    return update(state, {
      receiptHeader: {
        isLoading: {$set: false},
        data: { $set: action.payload },
        isSuccess: { $set: true },
        isError: { $set: false },
        message: { $set: "" },
        selectedReceiptHeader: { $set: false }
      }
  });
}

const selectReceiptHeader = (state, action) => {
    return update(state, {
      receiptHeader: {
        selectedReceiptHeader: { $set: action.payload }
      }
  });
}


export default handleActions({
  [constants.LIST_RECEIPT_HEADER_REQUEST]: listReceiptHeaderRequest,
  [constants.LIST_RECEIPT_HEADER_SUCCESS]: listReceiptHeaderSuccess,
  [constants.SELECT_RECEIPT_HEADER]: selectReceiptHeader,
}, initialState);