import {call, put} from 'redux-saga/effects';
import * as actions from '../actions';
import {fireAjax} from '../../services';

export function* listReceiptHeaderRequest (action) {
    // 70a7c2cc-17bc-4697-acdd-dbde14e002fd
    // 6be7c05c-e300-432c-9960-c1c7983722c3
    // 6be7c05c-e300-432c-9960-c1c7983722c3
    //4be70ef6-f0e6-4524-9a8a-e1f0091a060a 
    try { 
        const response = yield call(fireAjax, 'POST', '/poc-management/list-receipt-header-by-company-period', {
            // companyPeriodRef:action.payload.companyPeriodRef
            companyPeriodRef:action.payload.id
        });

        if( response.data ){
            yield put(actions.listReceiptHeaderSuccess(response.data));
            if(action.payload && action.payload.fireDefaultRowDetailsRequest){
                let defaultReceiptHeader = response.data[0] || false;
                if( defaultReceiptHeader != false ){
                    yield put(actions.listReceiptRowDetailsRequest(defaultReceiptHeader))
                }
                
            }
            
        }
        // if (response.error === 0) {
        //   let {token, userid} = response.data;
        //   yield put(actions.userLoginSuccess());
        // } else if (response.error === 1) {
        //   yield put(actions.userLoginError());
        // }
    } catch (e) {
        console.log(e)
        // yield put(actions.userLoginError('Error Occurs !!'));
        // console.warn('Some error found in "logingRequest" action\n', e);
    }
}