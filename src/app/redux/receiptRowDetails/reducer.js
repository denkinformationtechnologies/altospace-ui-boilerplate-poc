import { handleActions } from "redux-actions";
import update from "immutability-helper";
import * as constants from "../constants";

let initialState = {
  receiptRowDetails: {
    isLoading: false,
    data: [],
    isSuccess: false,
    isError: false,
    message: "",
    isModalAccountPlanSearchMode: false
  }
};

const listReceiptRowDetailsRequest = (state, action) => {
  return update(state, {
    receiptRowDetails: {
      isLoading: { $set: true },
      data: { $set: [] },
      isSuccess: { $set: false },
      isError: { $set: false },
      message: { $set: "" }
    }
  });
};

const listReceiptRowDetailsSuccess = (state, action) => {
  return update(state, {
    receiptRowDetails: {
      isLoading: { $set: false },
      data: { $set: action.payload },
      isSuccess: { $set: true },
      isError: { $set: false },
      message: { $set: "" }
    }
  });
};

const doAccountPlanSearchModeOn = (state, action) => {
  return update(state, {
    receiptRowDetails: {
      isModalAccountPlanSearchMode: { $set: action.payload }
    }
  });
};

const doAccountPlanSearchModeOff = (state, action) => {
  return update(state, {
    receiptRowDetails: {
      isModalAccountPlanSearchMode: { $set: false }
    }
  });
};

const updateAccountCodeInReceiptRowDetails = (state, action) => {
  const {accountPlan, rowDetails} = action.payload
  let { data } = state.receiptRowDetails;
  data.map(row => {
    row['accountCode'] = row['accountCode'] || ""
    if(row.receiptRowRef == rowDetails.receiptRowRef ){
      row['accountCode'] = accountPlan['accountCode']
    }
    return row;
  });

  return update(state, {
    receiptRowDetails: {
      data: { $set: data },
      isModalAccountPlanSearchMode: { $set: false}
    }
  });
};

const updateReceiptRowDetailsSuccess = (state, action) => {
  return update(state, {
    receiptRowDetails: {
      isModalAccountPlanSearchMode: { $set: false}
    }
  });
}

export default handleActions(
  {
    [constants.LIST_RECEIPT_ROW_DETAILS_REQUEST]: listReceiptRowDetailsRequest,
    [constants.LIST_RECEIPT_ROW_DETAILS_SUCCESS]: listReceiptRowDetailsSuccess,
    [constants.DO_ACCOUNT_PLAN_SEARCH_MODE_ON]: doAccountPlanSearchModeOn,
    [constants.DO_ACCOUNT_PLAN_SEARCH_MODE_OFF]: doAccountPlanSearchModeOff,
    [constants.UPDATE_ACCOUNT_CODE_IN_RECEIPT_ROW_DETAILS]: updateAccountCodeInReceiptRowDetails,
    [constants.UPDATE_RECEIPT_ROW_DETAILS_SUCCESS]: updateReceiptRowDetailsSuccess,
  },
  initialState
);
