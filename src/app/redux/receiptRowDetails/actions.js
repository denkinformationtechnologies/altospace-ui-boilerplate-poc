import {call, put} from 'redux-saga/effects';
import * as actions from '../actions';
import {fireAjax} from '../../services';

export function* listReceiptRowDetailsRequest (action) {
    try {
        const response = yield call(
            fireAjax, 
            'POST', 
            // '/poc-management/list-receipt-row-by-receipt-header', 
            '/poc-management/search-receipt-row?search=true',
            {
                receiptHeaderRef: action.payload.receiptHeaderId || ""
            }
        );
        if( response.data ){
            yield put(actions.listReceiptRowDetailsSuccess(response.data));
        }
        // if (response.error === 0) {
        //   let {token, userid} = response.data;
        //   yield put(actions.userLoginSuccess());
        // } else if (response.error === 1) {
        //   yield put(actions.userLoginError());
        // }
    } catch (e) {
        console.log(e)
        // yield put(actions.userLoginError('Error Occurs !!'));
        // console.warn('Some error found in "logingRequest" action\n', e);
    }
}

export function* updateReceiptRowDetailsRequest (action) {
    try {
        const response = yield call(fireAjax, 'PUT', '/poc-management/update-receipt-row', 
            action.payload
        );
        if( response.data ){
            yield put(actions.updateReceiptRowDetailsSuccess(response.data));
            if(action.payload.receiptHeaderRef) {
                yield put(actions.listReceiptRowDetailsRequest({ receiptHeaderId: action.payload.receiptHeaderRef }));
            }
        }
        // if (response.error === 0) {
        //   let {token, userid} = response.data;
        //   yield put(actions.userLoginSuccess());
        // } else if (response.error === 1) {
        //   yield put(actions.userLoginError());
        // }
    } catch (e) {
        console.log(e)
        yield put(actions.updateReceiptRowDetailsError('Error Occurs !!'));
        // console.warn('Some error found in "logingRequest" action\n', e);
    }
}