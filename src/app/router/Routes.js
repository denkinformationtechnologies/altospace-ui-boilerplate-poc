/**
 * High level router.
 *
 * Note: It's recommended to compose related routes in internal router
 * components (e.g: `src/pages/auth/AuthPage`, `src/pages/home/HomePage`).
 */

import React from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import { useLastLocation } from "react-router-last-location";
import HomePage from "../pages/home/HomePage";
import AuthPage from "../pages/auth/AuthPage";
import ErrorsPage from "../pages/errors/ErrorsPage";
import LogoutPage from "../pages/auth/Logout";
import { LayoutContextProvider } from "../../_metronic";
import Layout from "../../_metronic/layout/Layout";
import * as services from "../services/index"
import * as routerHelpers from "../router/RouterHelpers";
import AuthenticateRouter from "./AuthenticateRouter";

export const Routes = withRouter(({ history }) => {
  const lastLocation = useLastLocation();
  routerHelpers.saveLastLocation(lastLocation);
  const { isAuthorized, menuConfig, userLastLocation, auth , authReducer } = useSelector(
    ({ auth, urls, authReducer,builder: { menuConfig }}) => ({
      menuConfig,
      auth,
      authReducer,
      isAuthorized: authReducer.login.isLoggedIn,
      userLastLocation: routerHelpers.getLastLocation(),
    }),
    shallowEqual
  );
  const isLoggedIn = services.getDataFromLocalStroage('token') ? true : false
  const location = "/listcompany";
  return (
    /* Create `LayoutContext` from current `history` and `menuConfig`. */
    <LayoutContextProvider history={history} menuConfig={menuConfig}>
    <AuthenticateRouter>
      <Switch>
        {!isLoggedIn ? (
          <Route path="/auth/login" component={AuthPage} />
        ) : (
          <Redirect from="/auth" to={location} />
        )} 

        <Route path="/error" component={ErrorsPage} />
        <Route path="/logout" component={LogoutPage} />

        {!isLoggedIn ? (
          <Redirect to="/auth/login" />
        ) : ( 
            <Layout>
              <HomePage userLastLocation={location} />
            </Layout>
        )}
      </Switch>
    </AuthenticateRouter>
    </LayoutContextProvider>
  );
});
