import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {Routes} from "./Routes";
import * as services from "../services/index";
import {actions} from "../redux/ducks/auth.duck";
import { connect } from "react-redux";

class AuthenticateRouter extends Component {

  render() {

    return <div>{this.props.children}</div>;
  }
}

const mapStateToProps = state => {   

  return {
    auth_detail: state.authReducer.login                                                                                                              
  };
};

const mapDispatchToProps = dispatch => ({
    restoreTokenData: () => {
    dispatch(actions.restoreTokenData());
    }
    
   });
   export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthenticateRouter ));

